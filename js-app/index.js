const getConfig = require('./lib/config/config')
const startServer = require('./lib/server')

const multipartConfig = {
  limits: {
    fileSize: Math.pow(2, 26),
    files: 1
  }
}

async function main () {
  process.on('unhandledRejection', (err) => {
    console.error(err)
    process.exit(1)
  })
  const config = await getConfig()

  const server = require('fastify')(config.fastifyInit)
  server.register(require('@fastify/multipart'), multipartConfig)
  server.register(startServer, config)

  await server.listen(config.fastify)
}

main()
