'use strict'

const fp = require('fastify-plugin')
const knex = require('knex')

async function fastifyKnexJS (fastify, options, next) {
  try {
    const handler = knex(options.knex)
    await handler.migrate.latest()

    fastify
      .decorate('knex', handler)
      .addHook('onClose', (instance, done) => {
        if (instance.knex === handler) {
          instance.knex.destroy()
          delete instance.knex
        }

        done()
      })

    next()
  } catch (err) {
    next(err)
  }
}

module.exports = fp(fastifyKnexJS, '>=0.30.0')
