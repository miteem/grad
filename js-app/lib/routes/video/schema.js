const S = require('fluent-json-schema')

const create = {
  consumes: ['multipart/form-data'],
  response: {
    201: S.null(),
    400: S.object()
      .prop('message', S.string().required())
      .prop('allowed_type', S.string().required())
  }
}

const listIds = {
  querystring: S.object()
    .prop('page', S.number())
    .prop('size', S.number()),
  response: {
    200: S.object()
      .prop('page', S.number().required())
      .prop('page_size', S.number().required())
      .prop('content', S.array().items(S.string()).required())
  }
}

const getPartialVideo = {
  params: S.object()
    .prop('uuid', S.string().format('uuid').required()),
  headers: S.object()
    .prop('range', S.string().required()),
  response: {
    206: S.array().items(S.integer())
  }
}

module.exports = { create, listIds, getPartialVideo }
