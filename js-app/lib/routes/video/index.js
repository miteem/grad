const fp = require('fastify-plugin')
const schema = require('./schema')

const VIDEO_BUFFER = Math.pow(2, 20)

async function videos (server, options, done) {
  const videosModel = require('../../models/videos')(server.knex)

  server.route({
    method: 'POST',
    path: '/',
    schema: schema.create,
    handler: async function (req, reply) {
      const data = await req.file()

      if (!/^video\/.+$/.test(data.mimetype)) {
        return reply
          .code(400)
          .send({ message: 'mime type not allowed', allowed_type: 'video/*' })
      }

      const buffer = await data.toBuffer()
      const video = {
        mime: data.mimetype,
        size: buffer.length,
        bytes: buffer
      }

      const id = await videosModel.create(video)

      reply
        .code(201)
        .header('location', `/${id}`)
    }
  })

  server.route({
    method: 'GET',
    path: '/ids',
    schema: schema.listIds,
    handler: async function (req, reply) {
      return await videosModel.getIds(req.query)
    }
  })

  server.route({
    method: 'GET',
    path: '/:uuid',
    schema: schema.getPartialVideo,
    handler: async function (req, reply) {
      const uuid = req.params.uuid
      const rangeHeader = req.headers.range

      const result = getFirstRange(rangeHeader)

      if (result === -1) {
        return reply
          .code(400)
          .send({ message: 'invalid range value' })
      }
      const start = result[0] + 1
      const count = result[1] - result[0]

      const video = await videosModel.findPartialVideo(uuid, start, count)

      if (!video) {
        return reply
          .code(400)
          .send({ message: `cant find video with id ${uuid}` })
      }

      const contentRange = `bytes ${result[0]}-${result[0] + video.bytes.length}/${video.size}`
      reply
        .code(206)
        .headers({
          'Accept-Ranges': 'bytes',
          'Content-Type': video.mime,
          'Content-Range': contentRange,
          'Content-Length': video.bytes.length
        })
        .send(video.bytes)
    }
  })

  done()
}

function getFirstRange (ranges) {
  ranges = ranges.toLowerCase()

  if (!/^bytes=((\d+-|\d+-\d+|-\d+),?)+$/.test(ranges)) {
    return -1
  }

  ranges = ranges.replace(/(bytes=|[^\d,-])/g, '')
  const range = ranges.split(',')[0]
  return parseRange(range)
}

function parseRange (range) {
  const dashIdx = range.indexOf('-')

  let start
  let end

  if (dashIdx === 0) {
    start = parseInt(range.substring(1))
    end = Number.MAX_SAFE_INTEGER

    return [start, end]
  }

  const parts = range.split('-')

  start = parseInt(parts[0])
  end = start + VIDEO_BUFFER

  if (parts.length !== 1 && /\d+/.test(parts[1])) {
    end = parseInt(parts[1])

    if (start >= end) {
      return -1
    }
  }

  return [start, end]
}

module.exports = fp(videos)
