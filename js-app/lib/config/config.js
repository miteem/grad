const envSchema = require('env-schema')
const S = require('fluent-json-schema')
const knexconf = require('../../knexfile')

async function getConfig () {
  const env = envSchema({
    dotenv: true,
    schema: S.object()
      .prop('NODE_ENV', S.string().required())
      .prop('JS_ADDRESS', S.string().default('localhost'))
      .prop('JS_PORT', S.string().default('8002'))
      .prop(
        'LOG_LEVEL',
        S.string()
          .enum(['fatal', 'error', 'warn', 'info', 'debug', 'trace', 'silent'])
          .default('info')
      )
  })

  const config = {
    fastify: {
      host: env.JS_ADDRESS,
      port: env.JS_PORT
    },
    fastifyInit: {
      // disableRequestLogging: true,
      logger: {
        level: env.LOG_LEVEL,
        serializers: {
          req: (request) => ({
            method: request.raw.method,
            url: request.raw.url,
            hostname: request.hostname
          }),
          res: (response) => ({
            statusCode: response.statusCode
          })
        }
      }
    },
    knex: knexconf[env.NODE_ENV]
  }

  return config
}

module.exports = getConfig
