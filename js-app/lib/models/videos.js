/**
 * @param { import("knex").Knex } knex
 */
module.exports = function (knex) {
  return {
    create: async function (video) {
      const result = await knex('videos')
        .returning('id')
        .insert({
          mime: video.mime,
          size: video.size,
          bytes: video.bytes
        })

      return result[0].id
    },

    getIds: async function (filters) {
      const offset = Math.abs(filters?.page || 0)
      const limit = Math.abs(filters?.size || 20)

      const ids = await knex('videos')
        .select('id')
        .orderBy('created_at', 'asc')
        .offset(offset)
        .limit(limit)

      return {
        page: offset,
        page_size: limit,
        content: ids.map(ob => ob.id)
      }
    },

    findPartialVideo: async function (uuid, start, count) {
      const video = await knex('videos')
        .select('id', 'mime', 'size', knex.raw('SUBSTRING(bytes FROM ? FOR ?) AS bytes', [start, count]))
        .where('id', uuid)
        .first()

      return video && {
        id: video.id,
        mime: video.mime,
        size: video.size,
        bytes: video.bytes
      }
    }

  }
}
