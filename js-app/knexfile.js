const envSchema = require('env-schema')
const S = require('fluent-json-schema')

const env = envSchema({
  dotenv: true,
  schema: S.object()
    .prop('DB_USER', S.string().required())
    .prop('DB_PASS', S.string().required())
    .prop('DB_URL', S.string().required())
    .prop('DB_PORT', S.integer().required())
    .prop('DB_DATABASE', S.string().required())
})

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
const configs = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: './dev.sqlite3' // use ':memory:' for in-memory db
    },
    migrations: {
      directory: './knex/migrations'
    },
    useNullAsDefault: true
  },

  production: {
    client: 'pg',
    connection: {
      host: env.DB_URL,
      port: env.DB_PORT,
      user: env.DB_USER,
      password: env.DB_PASS,
      database: env.DB_DATABASE
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: './knex/migrations',
      tableName: 'knex_migrations'
    }
  }
}

module.exports = configs
