/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema

    .createTable('videos', function (table) {
      table
        .uuid('id')
        .unique()
        .primary()
        .defaultTo(knex.fn.uuid())
        .notNullable()
      table
        .string('mime')
        .notNullable()
      table
        .integer('size')
        .notNullable()
      table
        .binary('bytes')
        .notNullable()
      table.timestamps(true, true)
    })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema
    .dropTableIfExists('videos')
}
