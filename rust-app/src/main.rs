use actix_cors::Cors;
use actix_web::{middleware::Logger, web::Data, App, HttpServer};
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use dotenv::dotenv;
use env_logger::Env;
use std::env;

mod models;
mod schema;
mod video_api;

type DbPool = Pool<ConnectionManager<PgConnection>>;

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let socket_addr = env::var("RUST_ADDRESS").unwrap_or("127.0.0.1".to_string());
    let socket_port: u16 = env::var("RUST_PORT")
        .unwrap_or("8000".to_string())
        .parse::<u16>()
        .expect("Invalid port number");

    env_logger::init_from_env(Env::default().default_filter_or("info"));

    let pool = db_pool();

    HttpServer::new(move || {
        let cors = Cors::permissive();

        App::new()
            .app_data(Data::new(pool.clone()))
            .wrap(cors)
            .wrap(Logger::default())
            .configure(video_api::config)
    })
    .bind((socket_addr, socket_port))?
    .run()
    .await
}

fn db_pool() -> Pool<ConnectionManager<PgConnection>> {
    let user = env::var("DB_USER").expect("DB_USER must be set");
    let password = env::var("DB_PASS").expect("DB_PASS must be set");
    let url = env::var("DB_URL").expect("DB_URL must be set");
    let port = env::var("DB_PORT").expect("DB_PORT must be set");
    let database = env::var("DB_DATABASE").expect("DB_DATABASE must be set");

    let database_url = format!(
        "postgres://{}:{}@{}:{}/{}",
        user, password, url, port, database
    );

    let manager = ConnectionManager::<PgConnection>::new(database_url);

    let pool = Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    pool.get()
        .unwrap() // here we are getting connection
        .run_pending_migrations(MIGRATIONS)
        .expect("Can't run migration");

    pool
}
