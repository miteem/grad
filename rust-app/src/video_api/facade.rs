use std::str::FromStr;

use actix_multipart::Multipart;
use actix_web::{
    http::header::{self, ByteRangeSpec::*, ContentRange, ContentRangeSpec, HeaderValue, Range},
    web::{self, Data, Path, Query},
    HttpRequest, HttpResponse, Responder,
};
use futures_util::TryStreamExt;
use uuid::Uuid;

use crate::{
    models::video::{db, Video},
    DbPool,
};

use super::{dto::Page, req::Page as ReqPage, VIDEO_BUFFER};

pub async fn video_upload(mut payload: Multipart, pool: Data<DbPool>) -> impl Responder {
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_type = field.content_type();

        if !content_type.is_some_and(|mime| mime.type_() == mime::VIDEO) {
            continue;
        }

        let mime = content_type.unwrap().clone();

        let mut bytes = Vec::new();

        while let Ok(Some(chunk)) = field.try_next().await {
            bytes.extend_from_slice(&chunk);
        }

        let query_result = web::block(move || {
            let mut conn = pool.get().expect("couldn't get db connection from pool");

            db::create_video(&mut conn, mime, bytes.len().try_into().unwrap(), bytes)
        })
        .await
        .unwrap();

        return match query_result {
            Ok(uuid) => HttpResponse::Created()
                .insert_header((header::LOCATION, format!("/{}", uuid)))
                .finish(),
            Err(err) => HttpResponse::BadRequest().body(err.to_string()),
        };
    }

    HttpResponse::BadRequest().finish()
}

pub async fn video_view(
    path: Path<(String,)>,
    pool: Data<DbPool>,
    req: HttpRequest,
) -> impl Responder {
    let id = Uuid::from_str(&path.0);

    let id = match id {
        Err(err) => return HttpResponse::BadRequest().body(err.to_string()),
        Ok(uuid) => uuid,
    };

    let range = get_range(req.headers().get(header::RANGE).unwrap());

    let (start, end) = match range {
        Some(r) => r,
        _ => return HttpResponse::BadRequest().body("Invalid range value"),
    };

    if start >= end {
        return HttpResponse::RangeNotSatisfiable().finish();
    }

    let query_result = web::block(move || {
        let mut conn = pool.get().expect("couldn't get db connection from pool");

        db::find_video_part(&mut conn, id, start + 1, end - start)
    })
    .await
    .unwrap();

    match query_result {
        Err(err) => HttpResponse::BadRequest().body(err.to_string()),
        Ok(vid) => build_video_view_response(vid, start),
    }
}

fn get_range(range: &HeaderValue) -> Option<(u64, u64)> {
    let range = Range::from_str(range.to_str().unwrap_or(""));

    let range_specs = match range {
        Ok(Range::Bytes(rs)) => rs,
        _ => return None,
    };

    let range = match range_specs[0] {
        From(s) => (s, s + VIDEO_BUFFER),
        FromTo(s, e) => (s, e),
        Last(e) => (e, u64::MAX),
    };

    Some(range)
}

fn build_video_view_response(vid: Video, start: u64) -> HttpResponse {
    let Video {
        id: _,
        mime,
        size,
        bytes,
        created_at: _,
    } = vid;

    let part_size = bytes.len();

    let content_range = ContentRangeSpec::Bytes {
        range: Some((start, start + (part_size as u64))),
        instance_length: Some(size as u64),
    };

    HttpResponse::PartialContent()
        .insert_header(ContentRange(content_range))
        .insert_header((header::ACCEPT_RANGES, "bytes"))
        .insert_header((header::CONTENT_LENGTH, part_size))
        .insert_header((header::CONTENT_TYPE, mime))
        .body(bytes)
}

pub async fn video_ids_list(pool: Data<DbPool>, page: Query<ReqPage>) -> impl Responder {
    let ReqPage { page, size } = page.into_inner();
    let page = page.unwrap();
    let size = size.unwrap();

    let query_result = web::block(move || {
        let mut conn = pool.get().expect("couldn't get db connection from pool");
        db::get_video_ids(&mut conn, page, size)
    })
    .await
    .unwrap();

    match query_result {
        Err(err) => HttpResponse::BadRequest().body(err.to_string()),
        Ok(ids) => HttpResponse::Ok().json(Page::new(page, size, ids)),
    }
}
