use actix_web::{guard::GuardContext, http::header::Range};

pub fn video_view_gard(ctx: &GuardContext) -> bool {
    matches!(ctx.header::<Range>(), Some(Range::Bytes(_)))
}
