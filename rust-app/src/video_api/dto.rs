use serde::Serialize;

#[derive(Serialize)]
pub struct Page<T> {
    page: u32,
    page_size: u32,
    content_len: u32,
    content: Vec<T>,
}

impl<T> Page<T> {
    pub fn new(page: u32, page_size: u32, content: Vec<T>) -> Page<T> {
        Page {
            page,
            page_size,
            content_len: content.len() as u32,
            content,
        }
    }
}
