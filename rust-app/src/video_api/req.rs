use serde::Deserialize;

#[derive(Deserialize)]
pub struct Page {
    #[serde(default = "default_page")]
    pub page: Option<u32>,
    #[serde(default = "default_size")]
    pub size: Option<u32>,
}

fn default_page() -> Option<u32> {
    Some(0)
}

fn default_size() -> Option<u32> {
    Some(20)
}
