// @generated automatically by Diesel CLI.

diesel::table! {
    videos (id) {
        id -> Text,
        mime -> Text,
        size -> Integer,
        bytes -> Binary,
        created_at -> Timestamp,
    }
}
