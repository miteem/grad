use actix_multipart::Multipart;
use actix_web::{
    get, post,
    web::{self, Data, Path, Query},
    HttpRequest, Responder,
};

use crate::{video_api::req::Page as ReqPage, DbPool};

mod dto;
mod facade;
mod guard;
mod req;

const VIDEO_BUFFER: u64 = 2u64.pow(20);

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(video_upload_handler)
        .service(video_ids_list_handler)
        .service(video_view_handler);
}

#[post("/")]
async fn video_upload_handler(payload: Multipart, pool: Data<DbPool>) -> impl Responder {
    facade::video_upload(payload, pool).await
}

#[get("/{video_id}", guard = "guard::video_view_gard")]
async fn video_view_handler(
    path: Path<(String,)>,
    pool: Data<DbPool>,
    req: HttpRequest,
) -> impl Responder {
    facade::video_view(path, pool, req).await
}

#[get("/ids")]
async fn video_ids_list_handler(pool: Data<DbPool>, page: Query<ReqPage>) -> impl Responder {
    facade::video_ids_list(pool, page).await
}
