pub mod db;

use chrono::NaiveDateTime;
use diesel::prelude::*;
use mime::Mime;
use uuid::Uuid;

#[derive(Queryable, Selectable, QueryableByName)]
#[diesel(table_name = crate::schema::videos)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Video {
    pub id: String,
    pub mime: String,
    pub size: i32,
    pub bytes: Vec<u8>,
    pub created_at: NaiveDateTime,
}

#[derive(Insertable)]
#[diesel(table_name = crate::schema::videos)]
pub struct NewVideo {
    pub id: String,
    pub mime: String,
    pub size: i32,
    pub bytes: Vec<u8>,
}

impl NewVideo {
    pub fn new(mime: Mime, size: i32, bytes: Vec<u8>) -> NewVideo {
        NewVideo {
            id: Uuid::new_v4().to_string(),
            size,
            bytes,
            mime: mime.to_string(),
        }
    }
}
