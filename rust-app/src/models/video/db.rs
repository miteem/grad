use diesel::{ExpressionMethods, PgConnection, QueryDsl, QueryResult, RunQueryDsl};
use mime::Mime;
use uuid::Uuid;

use crate::models::video::{NewVideo, Video};

pub fn create_video(
    conn: &mut PgConnection,
    mime: Mime,
    size: i32,
    bytes: Vec<u8>,
) -> QueryResult<String> {
    use crate::schema::videos;

    let new_video = NewVideo::new(mime, size, bytes);

    diesel::insert_into(videos::table)
        .values(&new_video)
        .returning(videos::id)
        .get_result::<String>(conn)
}

pub fn find_video_part(
    conn: &mut PgConnection,
    id: Uuid,
    start: u64,
    count: u64,
) -> QueryResult<Video> {
    let query = format!("select id, mime, size, created_at, substring(bytes from {} for {}) as bytes from videos where id = '{}'", start, count, id);

    diesel::sql_query(query).get_result::<Video>(conn)
}

pub fn get_video_ids(conn: &mut PgConnection, page: u32, size: u32) -> QueryResult<Vec<String>> {
    use crate::schema::videos;

    videos::table
        .select(videos::id)
        .order(videos::created_at.asc())
        .limit(size as i64)
        .offset((page * size) as i64)
        .load::<String>(conn)
}
