-- Your SQL goes here
create table videos (
    id varchar primary key not null,
    mime varchar not null,
    size integer not null,
    bytes bytea not null,
    created_at timestamp not null default CURRENT_TIMESTAMP
)