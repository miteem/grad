package com.example.javaapp.repo;

import com.example.javaapp.model.entity.Video;
import com.example.javaapp.model.projection.PartialVideoView;
import com.example.javaapp.model.projection.VideoIdView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface VideoRepo extends JpaRepository<Video, String> {

    @Query("select v.id as id from Video v")
    Page<VideoIdView> findAllIds(Pageable pageable);

    @Query(value = "select v.id as id, " +
            "v.mime as mime, " +
            "v.size as total, " +
            "substring(v.bytes from :start for :count) as bytes " +
            "from videos v where v.id = :uuid", nativeQuery = true)
    Optional<PartialVideoView> findPartialVideo(String uuid, int start, int count);
}
