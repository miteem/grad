package com.example.javaapp.web;

import com.example.javaapp.model.entity.Video;
import com.example.javaapp.service.VideoService;
import com.example.javaapp.service.model.PartialVideo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class VideoController {
    final VideoService videoService;

    @PostMapping
    public ResponseEntity<?> videoUpload(MultipartFile video) {
        try {
            Video vid = videoService.create(video);
            return ResponseEntity.created(URI.create("/" + vid.getId())).build();
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("/ids")
    public ResponseEntity<?> videoIdsList(Pageable pageable) {
        try {
            var ids = videoService.getAllIds(pageable);
            return ResponseEntity.ok(ids);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<?> videoView(
            @PathVariable String uuid,
            @RequestHeader(HttpHeaders.RANGE) String ranges
    ) {
        try {
            Optional<PartialVideo> video = videoService.getPartialVideo(uuid, ranges);

            if (video.isPresent()) {
                PartialVideo vid = video.get();
                HttpHeaders headers = getVideoViewHeaders(vid);
                return ResponseEntity.status(206).headers(headers).body(vid.getBytes());
            }

            return ResponseEntity.badRequest().body("Cannot find video with id " + uuid);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    HttpHeaders getVideoViewHeaders(PartialVideo vid) {
        String contentRange =
                String.format("bytes %d-%d/%d", vid.getPartStart(), vid.getPartEnd(), vid.getSize());

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT_RANGES, "bytes");
        headers.set(HttpHeaders.CONTENT_TYPE, vid.getMime());
        headers.set(HttpHeaders.CONTENT_RANGE, contentRange);
        headers.set(HttpHeaders.CONTENT_LENGTH, String.valueOf(vid.getBytes().length));
        return headers;
    }
}
