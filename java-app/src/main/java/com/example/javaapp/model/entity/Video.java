package com.example.javaapp.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "videos")
@Entity
public class Video {
    @GeneratedValue(strategy = GenerationType.UUID)
    @Id
    String id;

    String mime;

    int size;

    byte[] bytes;

    @Version
    OffsetDateTime created_at;
}
