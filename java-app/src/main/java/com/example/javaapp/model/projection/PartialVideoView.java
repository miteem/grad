package com.example.javaapp.model.projection;

public interface PartialVideoView {
    String getId();

    String getMime();

    Integer getTotal();

    byte[] getBytes();
}
