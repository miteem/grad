package com.example.javaapp.model.projection;

public interface VideoIdView {
    String getId();
}
