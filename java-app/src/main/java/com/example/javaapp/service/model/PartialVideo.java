package com.example.javaapp.service.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
@Getter
public class PartialVideo {
    String id;
    String mime;
    int size;
    int partStart;
    int partEnd;
    byte[] bytes;
}
