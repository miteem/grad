package com.example.javaapp.service;

import com.example.javaapp.model.entity.Video;
import com.example.javaapp.service.model.PartialVideo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

public interface VideoService {
    Video create(MultipartFile multipartFile) throws IOException;

    Page<String> getAllIds(Pageable pageable);

    Optional<PartialVideo> getPartialVideo(String uuid, String ranges);
}
