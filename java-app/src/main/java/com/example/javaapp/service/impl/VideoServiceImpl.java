package com.example.javaapp.service.impl;

import com.example.javaapp.model.entity.Video;
import com.example.javaapp.model.projection.PartialVideoView;
import com.example.javaapp.model.projection.VideoIdView;
import com.example.javaapp.repo.VideoRepo;
import com.example.javaapp.service.VideoService;
import com.example.javaapp.service.model.PartialVideo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpRange;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
@Service
public class VideoServiceImpl implements VideoService {

    static final int VIDEO_BUFFER = (int) Math.pow(2, 20);

    final VideoRepo videoRepo;

    @Override
    public Video create(MultipartFile multipartFile) throws IOException {
        if (isNull(multipartFile)) {
            throw new RuntimeException("Invalid data.");
        }

        String contentType = multipartFile.getContentType();
        if (isNull(contentType) || !contentType.matches("video/.+")) {
            throw new RuntimeException("Invalid file content type");
        }

        Video video = Video.builder()
                .mime(contentType)
                .size((int) multipartFile.getSize())
                .bytes(multipartFile.getBytes())
                .build();

        return videoRepo.save(video);
    }

    @Override
    public Page<String> getAllIds(Pageable pageable) {
        Page<VideoIdView> ids = videoRepo.findAllIds(pageable);
        return ids.map(VideoIdView::getId);
    }

    @Override
    public Optional<PartialVideo> getPartialVideo(
            String uuid,
            String ranges
    ) {
        validateRanges(ranges);
        String rangeString = getFirstRange(ranges);
        int[] range = parseRange(rangeString);

        int start = range[0] + 1;
        int count = range[1] - range[0];
        Optional<PartialVideoView> partialVideo = videoRepo.findPartialVideo(uuid, start, count);

        return partialVideo.map(video -> new PartialVideo(
                video.getId(),
                video.getMime(),
                video.getTotal(),
                range[0],
                range[0] + video.getBytes().length,
                video.getBytes()
        ));
    }

    void validateRanges(String ranges) {
        HttpRange.parseRanges(ranges);
    }

    String getFirstRange(String ranges) {
        ranges = ranges.replaceAll("(bytes=|[^\\d,-])", "");
        return ranges.split(",")[0];
    }

    int[] parseRange(String range) {
        int dashIdx = range.indexOf(45);
        int start;
        int end;

        if (dashIdx == 0) {
            start = Integer.parseInt(range.substring(1));
            end = Integer.MAX_VALUE;

            return new int[]{start, end};
        }

        String[] parts = range.split("-");
        start = Integer.parseInt(parts[0]);
        end = start + VIDEO_BUFFER;

        if (parts.length != 1) {
            end = Integer.parseInt(parts[1]);

            if (start >= end) {
                throw new RuntimeException("Invalid range values");
            }
        }

        return new int[]{start, end};
    }
}
